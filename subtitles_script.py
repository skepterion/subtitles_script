


import os

directory_in_question = os.path.abspath(os.path.dirname(__file__))
# set the value below equal to the directory which contains the files you wish to be renamed
current_directory = os.path.join(directory_in_question, 'Season 3')

episodes_list = []
subtitles_list = []


# separate files into episodes and subtitles by appending them to different lists
def separate_files(directory_in_question, ep_list, sub_list):
    with os.scandir(directory_in_question) as it:
        for x in it:
            filename_in_question = os.path.basename(x)
            if 'srt' not in filename_in_question:
                ep_list.append(filename_in_question)
            if 'srt' in filename_in_question:
                sub_list.append(filename_in_question)


separate_files(current_directory, episodes_list, subtitles_list)

# sort the lists
episodes_list.sort()
subtitles_list.sort()

# change subtitle filename according to relevant episode filename
def sub_rename(directory_in_question):
    with os.scandir(directory_in_question) as it:
        for x in it:
            filename_in_question = os.path.basename(x)
            if filename_in_question in subtitles_list:  # check if file is a subtitle
                index_in_question = subtitles_list.index(filename_in_question)  # access the index that the subtitle file has in the subtitles_list
                os.rename(os.path.join(directory_in_question, filename_in_question), (os.path.join(directory_in_question, os.path.splitext(episodes_list[index_in_question])[0]) + '.srt'))
                # rename subtitle file
                # first part specifies subtitle file to be renamed, second part specifies episode file that the subtitle file should be named after
                # the 'os.path.splitext()[0]' part removes the file extension from the episode file
                # the ' + '.srt' ' part simply adds the relevant extension to the subtitle file


sub_rename(current_directory)
